//
//  AddGoalViewController.swift
//  3-sixty
//
//  Created by Mahmoud Elsayed on 27/09/2021.
//

import UIKit

class SelfSizedTableView: UITableView {
  var maxHeight: CGFloat = UIScreen.main.bounds.size.height
  
  override func reloadData() {
        super.reloadData()
        self.invalidateIntrinsicContentSize()
        self.layoutIfNeeded()
  }
  
  override var intrinsicContentSize: CGSize {
        setNeedsLayout()
        layoutIfNeeded()
        let height = min(contentSize.height, maxHeight)
        return CGSize(width: contentSize.width, height: height)
  }
}

class AddGoalViewController: UIViewController {
    
    // MARK: - Outlets
    @IBOutlet weak private var communityButton: UIButton!
    @IBOutlet weak private var processButton: UIButton!
    @IBOutlet weak private var skillsButton: UIButton!
    @IBOutlet weak private var tasksTableView: SelfSizedTableView!
    @IBOutlet weak private var otherButton: UIButton!
    @IBOutlet weak private var goalNameTextField: UITextField!
    @IBOutlet weak private var difficultyPointsTextField: UITextField!
    @IBOutlet weak private var otherCategoryTextField: UITextField!
    @IBOutlet weak private var addGoalButton: UIButton!

    // MARK: - Variables
    private let taskCellId = "TaskTableViewCell"
    private var goalCaetgory: String = ""
    
    // MARK: - Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        goalNameTextField.delegate = self
        otherCategoryTextField.delegate = self
        difficultyPointsTextField.delegate = self
        configTableView()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        // write title on top of screen
        let titleLabel = UILabel()
        titleLabel.text = "Add Goal"
        navigationItem.titleView = titleLabel
        
        
//        self.tasksTableView.rowHeight = UITableView.automaticDimension;
       self.tasksTableView.estimatedRowHeight = 70.0
        // making add goal button round
        addGoalButton.layer.cornerRadius = 25.0
        
        
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        tasksTableView.reloadData()
    }
    
    // MARK: - Methods
    static func create() -> AddGoalViewController{
        return AddGoalViewController(nibName: "AddGoalViewController", bundle: nil)
    }
    func configTableView(){
        let tasknib = UINib(nibName: taskCellId, bundle: nil)
        tasksTableView.register(tasknib, forCellReuseIdentifier: taskCellId)
        tasksTableView.dataSource = self
        tasksTableView.delegate = self
        
    }
    
    // if radio button pressed all other button must be disabled
    func enableButtonDisableRest(_ buttonnum:Int){
        let buttons = [communityButton,processButton,skillsButton,otherButton]
        for (index,button) in buttons.enumerated(){
            if index == buttonnum{
                button?.isSelected = true
            }
            else{
                button?.isSelected = false
            }
        }
    }
    //func for alertcontroller pop up
    func alertBox(title: String, message:String){
        let alert = UIAlertController(title: title, message: message,preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
        self.present(alert,animated: true)
    }
    
    // MARK: - IBActions
    @IBAction private func communitySelected(_ sender: Any) {
        enableButtonDisableRest(0)
        goalCaetgory = "community"
        otherCategoryTextField.isHidden = true
    }
    @IBAction private func processSelected(_ sender: Any) {
        enableButtonDisableRest(1)
        goalCaetgory = "process"
        otherCategoryTextField.isHidden = true
    }
    @IBAction private func skillsSelected(_ sender: Any) {
        enableButtonDisableRest(2)
        goalCaetgory = "skills"
        otherCategoryTextField.isHidden = true
    }
    @IBAction private func otherSelected(_ sender: Any) {
        enableButtonDisableRest(3)
        alertBox(title: "other category", message: "Please type new category name under 100 characters ")
        otherCategoryTextField.isHidden = false
    }
    @IBAction private func extractTaskAddingSheet(_ sender: Any) {
        //when pressed a task sheet pops from bottom to add subtask
        //MARK: TODO
        // then add cell to tasks list and reload data in tasks table view
        let taskview = AddTaskViewController.create()
        self.navigationController?.present(taskview, animated: true)
    }
    // when add goal pressed. check if user entered a goal name and a goal category (mandatory fields)
    @IBAction private func addGoalButtonPressed(_ sender: Any) {
        let goalName = goalNameTextField.text
        let points = difficultyPointsTextField.text ?? ""
        let difficultyNum = Int(points) ?? 0
        if goalCaetgory == ""{
            goalCaetgory = otherCategoryTextField.text ?? ""
        }
        if goalCaetgory == "" {
            alertBox(title: "Goal Caegory missing", message: "Please select one category ")
        }
        if goalName == ""{
            alertBox(title: "Goal name missing", message: "Please write your goal name ")
        }
        if difficultyNum > 40{
            alertBox(title: "Invalid difficulty points ", message: " difficulty points must be less than 40 ")
        }
        else{
            print("adding goal with name \(goalName ?? "default") and category \(goalCaetgory) and points = \(difficultyNum)")
            //MARK: TODO
            // post request of goal and navigation to previous screen
        }
    }
}
// MARK: - Extensions

// extension to limit goalname to 200 characters and difficulty pointy < 40
extension AddGoalViewController: UITextFieldDelegate{
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == goalNameTextField{
            // get the current text, or use an empty string if that failed
            let currentText = goalNameTextField.text ?? ""
            
            // attempt to read the range they are trying to change, or exit if we can't
            guard let stringRange = Range(range, in: currentText) else { return false }

            // add their new text to the existing text
            let updatedText = currentText.replacingCharacters(in: stringRange, with: string)

            // make sure the result is under 200 characters
            return updatedText.count <= 200
        }
        if textField == otherCategoryTextField{
            // get the current text, or use an empty string if that failed
            let currentText = otherCategoryTextField.text ?? ""
            
            // attempt to read the range they are trying to change, or exit if we can't
            guard let stringRange = Range(range, in: currentText) else { return false }

            // add their new text to the existing text
            let updatedText = currentText.replacingCharacters(in: stringRange, with: string)

            // make sure the result is under 200 characters
            return updatedText.count <= 100

        }
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField == difficultyPointsTextField{
            let points = difficultyPointsTextField.text ?? ""
            let num = Int(points) ?? 0
            if num  > 40 {
                difficultyPointsTextField.layer.borderColor = UIColor.red.cgColor
                alertBox(title: "Invalid difficulty points ", message: " difficulty points must be less than 40 ")
            }
            else{
                difficultyPointsTextField.layer.borderColor = UIColor.black.cgColor
            }
        }
    }
}
// Tasks tableview datasource and delegate
extension AddGoalViewController: UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 4
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: TaskTableViewCell = tasksTableView.dequeueReusableCell(withIdentifier: taskCellId) as! TaskTableViewCell
        cell.setData(taskNAme: "get Laundrey", deadline: "5/12/2021")
        return cell
    }
}
extension AddGoalViewController: UITableViewDelegate{
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70.00
        
    }
    
}
