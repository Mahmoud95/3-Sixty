//
//  TaskTableViewCell.swift
//  3-sixty
//
//  Created by Mahmoud Elsayed on 02/10/2021.
//

import UIKit

class TaskTableViewCell: UITableViewCell {

    @IBOutlet weak private var taskDeadlineLabel: UILabel!
    @IBOutlet weak private var taskNameLabel: UILabel!

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setData(taskNAme: String, deadline: String){
        taskNameLabel.text = taskNAme
        taskDeadlineLabel.text = deadline
        
    }
}
