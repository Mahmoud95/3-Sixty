//
//  LoginViewController.swift
//  3-sixty
//
//  Created by Zeina Desouky on 9/28/21.
//

import UIKit
import GoogleSignIn

class LoginViewController: UIViewController, GIDSignInDelegate {

    
    
    @IBOutlet weak var mainLogoLabel: UILabel!
    @IBOutlet weak var welcomeLabel: UILabel!
    @IBOutlet weak var signInButton: UIButton!
    @IBAction func googleSignInButton(_ sender: UIButton) {
        GIDSignIn.sharedInstance()?.presentingViewController = self
        
        GIDSignIn.sharedInstance()?.signIn()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        mainLogoLabel.font = UIFont(name:"Georgia", size: 50.0)
           welcomeLabel.font = UIFont(name:"Damascus", size: 30.0)
           GIDSignIn.sharedInstance()?.delegate = self
        signInButton.layer.cornerRadius = 25.0
        signInButton.backgroundColor = UIColor(red: 0.18, green: 0.5, blue: 0.93, alpha: 1.0)

        // Do any additional setup after loading the view.
    }

    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
        if error == nil {
                   
            let idToken = user.authentication.idToken
           // print(user.userID!)
           // print(idToken ?? " ")
               }
    }
    

}
