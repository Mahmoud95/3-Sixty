//
//  EditInformationViewController.swift
//  3-sixty
//
//  Created by Ali Amer on 06/10/2021.
//

import UIKit

class EditInformationViewController: UIViewController,UIPickerViewDelegate,UIPickerViewDataSource,UITextFieldDelegate {
    // Mark:- Outlets
    @IBOutlet weak var SaveButton: UIButton!
    @IBOutlet weak var PositionText: UITextField!
    @IBOutlet weak var TeamTextBox: UITextField!
    @IBOutlet weak var YearText: UITextField!
    @IBOutlet weak var MonthText: UITextField!
    @IBOutlet weak var EmailText: UITextField!
    @IBOutlet weak var AccountLabel: UILabel!
    // Mark:-Variables
    let teams = ["Backend","Frontend","IOS"]
    
    let months = ["January","Febuary","March","April","May","June","July","August","September","October","November","December"]
    var years = [2007]
  
    var year = 0
    var pickerView = UIPickerView()
    var DatePickerView = UIPickerView()
    var MonthPickerView  = UIPickerView()
    

    
    static func create() -> EditInformationViewController{
        return EditInformationViewController(nibName: "EditInformationViewController", bundle: nil)
    }

 
    
  //  @IBOutlet var views: UIView!
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        
        
        
       //
        //_ = Bundle.main.loadNibNamed("EditInformationViewController", owner: self, options: nil)?[0] as? UIView
        let col1 = UIColor(red: 0, green: 0.5, blue: 1, alpha: 1)
        SaveButton.backgroundColor = col1
        SaveButton.layer.cornerRadius=20
        SaveButton.setTitleColor(UIColor.white, for: .normal)
        calculateYears()
        let email = "Abdelwahab.m@gmail.com"
        pickerView.delegate = self
        pickerView.dataSource = self
        MonthPickerView.delegate = self
        MonthPickerView.dataSource = self
        DatePickerView.delegate = self
        DatePickerView.dataSource = self
        AccountLabel.numberOfLines = 3
        PositionText.delegate = self
        TeamTextBox.inputView = pickerView
        YearText.inputView = DatePickerView
        MonthText.inputView = MonthPickerView
        TeamTextBox.textAlignment = .left
        TeamTextBox.placeholder = "Choose Your Team"
        YearText.textAlignment = .left
        YearText.placeholder = "Year"
        MonthText.textAlignment = .left
        MonthText.placeholder = "Month"
        PositionText.textAlignment = .left
        PositionText.placeholder = "Write Your Position"
        EmailText.textAlignment = .left
        EmailText.placeholder = email
        EmailText.isUserInteractionEnabled = false
        
      
        
        

    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
            let currentText = textField.text ?? ""
            guard let stringRange = Range(range, in: currentText) else{ return false}
            
            let upDateText = currentText.replacingCharacters(in: stringRange, with: string)
            
            return upDateText.count < 50
        }
    
    
    
    
    @IBAction func SaveButtonPressed(_ sender: Any) {
    
    }

    
    
    
    func checkPositionCount(){
        
        let numCharacters = self.PositionText.text?.count ?? 0
        if(numCharacters > 50){
            
            self.PositionText.isEnabled=false;
        }
        
        else{
            self.PositionText.isEnabled=true;
        }
        
        
        
    }
    
    
    func calculateYears(){
        
         year = Calendar.current.component(.year, from: Date())
        
        
        var  i = 2008
       
        while(i<=year){
            
            years.append(i)

            i = i+1
        }
        
       
       
    }
    
  
    func numberOfComponents(in pickerView: UIPickerView) -> Int{
        return 1;
    }

    
    // returns the # of rows in each component..
 
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int

    {
        
        if(self.pickerView == pickerView){
            
        
            return teams.count
            
        }
        else if (DatePickerView==pickerView) {
        
            return years.count
        
        }
        else{
            return months.count;
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if(self.pickerView == pickerView){
            
            return teams[row]}
        
        else if (DatePickerView==pickerView){
          
            return String(years[row])
        }
        else{
            return months[row]
        }
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        if(self.pickerView == pickerView){
        TeamTextBox.text = teams[row]
        TeamTextBox.resignFirstResponder()}
        else if (DatePickerView==pickerView){
            YearText.text = String(years[row])
            YearText.resignFirstResponder()
        }
        
        else{
            MonthText.text = String(months[row])
            MonthText.resignFirstResponder()
            
        }
    }
    
    
    





  

}
