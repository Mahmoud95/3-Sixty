//
//  chooseTeam.swift
//  3-sixty
//
//  Created by Ali Amer on 29/09/2021.
//

import UIKit

class chooseTeam: UIViewController,UIPickerViewDelegate,UIPickerViewDataSource,UITextFieldDelegate {
    @IBOutlet weak var RegisterButton: UIButton!
    
    @IBOutlet weak var WelcomeText: UILabel!
    
    @IBOutlet weak var PositionText: UITextField!
    
    @IBOutlet weak var TeamTextBox: UITextField!
    
    @IBOutlet weak var YearText: UITextField!
    
    @IBOutlet weak var MonthText: UITextField!
    
    @IBOutlet weak var logo: UIImageView!
    let teams = ["Backend","Frontend","IOS"]
    
    let months = ["January","Febuary","March","April","May","June","July","August","September","October","November","December"]
    var years = [2007]
  
    var year = 0
    var pickerView = UIPickerView()
    var DatePickerView = UIPickerView()
    var MonthPickerView  = UIPickerView()
    
    var loginName = "Ali"
    
    static func create() -> chooseTeam{
        return chooseTeam(nibName: "chooseTeam", bundle: nil)
    }

 
    
  //  @IBOutlet var views: UIView!
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        
        
        
        _ = Bundle.main.loadNibNamed("chooseTeam", owner: self, options: nil)?[0] as? UIView
        addLogo()
        let col1 = UIColor(red: 0, green: 0.5, blue: 1, alpha: 1)
        RegisterButton.backgroundColor = col1
        RegisterButton.layer.cornerRadius=20
        RegisterButton.setTitleColor(UIColor.white, for: .normal)
        calculateYears()
        pickerView.delegate = self
        pickerView.dataSource = self
        MonthPickerView.delegate = self
        MonthPickerView.dataSource = self
        DatePickerView.delegate = self
        DatePickerView.dataSource = self
        WelcomeText.text="Welcome"+" "+loginName+","
        
        
        
        
        PositionText.delegate = self
        
        
        TeamTextBox.inputView = pickerView
        YearText.inputView = DatePickerView
        MonthText.inputView = MonthPickerView
        
        TeamTextBox.textAlignment = .left
        TeamTextBox.placeholder = "Choose Your Team"
        
        YearText.textAlignment = .left
        YearText.placeholder = "Year"
        
        MonthText.textAlignment = .left
        MonthText.placeholder = "Month"
        PositionText.textAlignment = .left
        PositionText.placeholder="Write Your Position"
        
        

    }

    
    func addLogo(){
               let yourImage: UIImage = UIImage(named: "logo")!
               logo.image = yourImage
           }

    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
            let currentText = textField.text ?? ""
            guard let stringRange = Range(range, in: currentText) else{ return false}
            
            let upDateText = currentText.replacingCharacters(in: stringRange, with: string)
            
            return upDateText.count < 50
        }
    
    
    
    
    @IBAction func RegisterButtonPressed(_ sender: Any) {
    

        
        if(TeamTextBox.text=="")
        {
            let alert = UIAlertController(title: "Invalid Team", message: "Team Field Cannot Be Empty.", preferredStyle: .alert)
            
            alert.addAction(UIAlertAction(title: "Got It!", style: .default, handler: nil))
            self.present(alert, animated: true)
            
        }
        
        else if(PositionText.text=="")
        {
            let alert = UIAlertController(title: "Invalid Position", message: "Position Field Cannot Be Empty.", preferredStyle: .alert)
            
            alert.addAction(UIAlertAction(title: "Got It!", style: .default, handler: nil))
            self.present(alert, animated: true)
            
        }
        else if(MonthText.text=="")
        {
            let alert = UIAlertController(title: "Invalid Month", message: "Month Field Cannot Be Empty.", preferredStyle: .alert)
            
            alert.addAction(UIAlertAction(title: "Got It!", style: .default, handler: nil))
            self.present(alert, animated: true)
            
        }
        else if(YearText.text=="")
        {
            let alert = UIAlertController(title: "Invalid Year", message: "Year Field Cannot Be Empty.", preferredStyle: .alert)
            
            alert.addAction(UIAlertAction(title: "Got It!", style: .default, handler: nil))
            self.present(alert, animated: true)
            
        }
        else{
            navigationController?.pushViewController(MyGoalsViewController.create(), animated: true)
        }
        
        
        
        
       
    }

    
    
    
    func checkPositionCount(){
        
        let numCharacters = self.PositionText.text?.count ?? 0
        if(numCharacters > 50){
            
            self.PositionText.isEnabled=false;
        }
        
        else{
            self.PositionText.isEnabled=true;
        }
        
        
        
    }
    
    
    func calculateYears(){
        
         year = Calendar.current.component(.year, from: Date())
        
        
        var  i = 2008
       
        while(i<=year){
            
            years.append(i)

            i = i+1
        }
        
       
       
    }
    
  
    func numberOfComponents(in pickerView: UIPickerView) -> Int{
        return 1;
    }

    
    // returns the # of rows in each component..
 
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int

    {
        
        if(self.pickerView == pickerView){
            
        
            return teams.count
            
        }
        else if (DatePickerView==pickerView) {
        
            return years.count
        
        }
        else{
            return months.count;
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if(self.pickerView == pickerView){
            
            return teams[row]}
        
        else if (DatePickerView==pickerView){
          
            return String(years[row])
        }
        else{
            return months[row]
        }
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        if(self.pickerView == pickerView){
        TeamTextBox.text = teams[row]
        TeamTextBox.resignFirstResponder()}
        else if (DatePickerView==pickerView){
            YearText.text = String(years[row])
            YearText.resignFirstResponder()
        }
        
        else{
            MonthText.text = String(months[row])
            MonthText.resignFirstResponder()
            
        }
    }
    
    
    



 

}
