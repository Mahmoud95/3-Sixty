//
//  CommentViewCell.swift
//  3-sixty
//
//  Created by John Gamil on 06/10/2021.
//

import UIKit

class CommentViewCell: UITableViewCell {
    
    @IBOutlet weak var displayImage : UIImageView!
    @IBOutlet weak var name : UILabel!
    @IBOutlet weak var date : UILabel!
    @IBOutlet weak var comment : UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func display(commentorName : String , date : String, commentorImage : UIImage, comment: String){
        name.text = commentorName
        self.date.text = date
        displayImage.image = commentorImage
        self.comment.text = comment
    }
}
