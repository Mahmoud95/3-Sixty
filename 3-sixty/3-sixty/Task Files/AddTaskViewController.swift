//
//  AddTaskViewController.swift
//  finalProject
//
//  Created by John Gamil on 27/09/2021.
//

import UIKit


class AddTaskViewController: UIViewController, UITextFieldDelegate {
    // MARK: - Outlets
    @IBOutlet weak var taskStatus: UIView!
    @IBOutlet weak var DetailstextField: UITextField!
    @IBOutlet weak var viewDate: UIView!
    @IBOutlet weak var dateSwitch: UISwitch!
    @IBOutlet weak var commentsView: UIView!
    @IBOutlet weak var toDoButton: UIButton!
    @IBOutlet weak var inProgressButton: UIButton!
    @IBOutlet weak var doneButton: UIButton!
    @IBOutlet weak var datePick: UIDatePicker!
    @IBOutlet weak var commentTextField: UITextField!
    @IBOutlet weak var commentsTableView: SelfSizedTableView!
    
    // MARK: - Variables
    private let editFlag = true
    private var date = ""
    private var status = 0
    private let commentCellID = "CommentViewCell"
    private let numofComments = 10
    
    
    
    // MARK: - Life Cycle
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(false)
        dateSwitch.isOn = false
        viewDate.isHidden = true
        taskStatus.isHidden = true
        commentsTableView.reloadData()
        self.commentsTableView.estimatedRowHeight = 70.0
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        DetailstextField.delegate = self
        let todaydate = datePick.date
        viewEditView()
        configTableView()
        
    }
    static func create() -> AddTaskViewController{
        return AddTaskViewController(nibName: "AddTaskViewController", bundle: nil)
    }
   // Populates the struct class
    @IBAction func addTask(_ sender: Any) {
        let taskEntries = entries(taskDetails: DetailstextField.text ?? "", status: status, date: date, comment: commentTextField.text!)
        print(taskEntries.taskDetails,taskEntries.status,taskEntries.date,taskEntries.comment)
        // MARK: - To DO
//        var request = URLRequest(url: url)
//        request.httpMethod = "POST"
//        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
//        request.setValue("application/json", forHTTPHeaderField: "")
//        let product = [
//            "details": taskEntries.taskDetails,
//            "todo_status": taskEntries.status,
//
//        ]
//        let productData = try! JSONSerialization.data(withJSONObject: product, options: [])
//        let taskPost = session.uploadTask(with: request, from: productData, completionHandler: {
//            data, response, error in
//            do {
//                  let json = try JSONSerialization.jsonObject(with: data!, options: [])
//              } catch {
//                  print("JSON error: \(error.localizedDescription)")
//              }
//        })
//        taskPost.resume()
        
    }
    // updates the check status
    func checkStauts(){
        if toDoButton.isSelected {
            status = 0
        }else if inProgressButton.isSelected {
            status = 1
        }else if doneButton.isSelected{
            status = 2
        }
    }
    
    func configTableView(){
        let commentViewNib = UINib(nibName: commentCellID, bundle: nil)
        commentsTableView.register(commentViewNib, forCellReuseIdentifier: commentCellID)
        commentsTableView.delegate = self
        commentsTableView.dataSource = self
    }
    // Updates the date to the date selected
    @IBAction func datePicker(_ sender: UIDatePicker) {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        date = formatter.string(from: datePick.date)
        
//        let dateString = datePick.date
//        print(dateString)
    }
    
    // Links the date to the switch
    @IBAction func viewDate(_ sender: Any) {
        viewDate.isHidden = !(dateSwitch.isOn)
        
    }
    
    // Sets the limit of the textfield to 100
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let currentText = DetailstextField.text ?? ""
        guard let stringRange = Range(range, in: currentText) else{ return false}
        let upDateText = currentText.replacingCharacters(in: stringRange, with: string)
        return upDateText.count < 100
    }
    // Differentiates between addTask and edit task
    func viewEditView(){
        taskStatus.isHidden = !editFlag
        commentsView.isHidden = !editFlag
    }
    //Checks if to Do is selected
    @IBAction func toDoRadio(_ sender: UIButton) {
        if sender.isSelected{
            sender.isSelected = false
            inProgressButton.isSelected = false
            doneButton.isSelected = false
        }else{
            sender.isSelected = true
            inProgressButton.isSelected = false
            doneButton.isSelected = false
        }
        checkStauts()
    }
    // Checks if In Progress is selected
    @IBAction func InProgressRadio(_ sender: UIButton) {
        if sender.isSelected{
            sender.isSelected = false
            toDoButton.isSelected = false
            doneButton.isSelected = false
        }else{
            sender.isSelected = true
            toDoButton.isSelected = false
            doneButton.isSelected = false
        }
        checkStauts()
    }
    // Checks if done is selected
    @IBAction func doneRadio(_ sender: UIButton) {
        if sender.isSelected{
            sender.isSelected = false
            toDoButton.isSelected = false
            inProgressButton.isSelected = false
        }else{
            sender.isSelected = true
            toDoButton.isSelected = false
            inProgressButton.isSelected = false
        }
        checkStauts()
    }

}
extension AddTaskViewController : UITableViewDelegate{
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
}
extension AddTaskViewController :UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return numofComments
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: CommentViewCell = commentsTableView.dequeueReusableCell(withIdentifier: commentCellID) as! CommentViewCell
       cell.display(commentorName: "John", date: "today", commentorImage: #imageLiteral(resourceName: "onBlackButton"), comment: "Please do your work")
        return cell
    }
    
}



// Struct class to collect entries
struct entries {
    var taskDetails: String
    var status: Int
    var date : String?
    var comment: String?
}


// Struct Class for the API call
struct tasks : Codable {
    let id : Int
    let details : String
    let todo_status : Int
    let deadline : Date
}
