//
//  NotificationsViewController.swift
//  3-sixty
//
//  Created by Mahmoud Elsayed on 11/10/2021.
//

import UIKit

class NotificationsViewController: UIViewController {

    @IBOutlet weak var notificationsTableView: SelfSizedTableView!
    
    private let notificationCellId = "NotificationsViewCell"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.notificationsTableView.rowHeight = UITableView.automaticDimension;
        self.notificationsTableView.estimatedRowHeight = 122
        configTable()
    }

    
    func configTable(){
        let notificaitonNib = UINib(nibName: notificationCellId, bundle: nil)
        notificationsTableView.register(notificaitonNib, forCellReuseIdentifier: notificationCellId)
        notificationsTableView.dataSource = self
        notificationsTableView.delegate = self
    }
}
extension NotificationsViewController: UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        4
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: NotificationsViewCell = notificationsTableView.dequeueReusableCell(withIdentifier: notificationCellId) as! NotificationsViewCell
        cell.setData(date: "September 09,2021 1:10 PM", message: "Someone commented on your task ")
        return cell
    }
    
    
}
extension NotificationsViewController:UITableViewDelegate{
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 122
    }
}
