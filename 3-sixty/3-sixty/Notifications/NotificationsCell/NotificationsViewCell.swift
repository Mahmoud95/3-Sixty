//
//  NotificationsViewCell.swift
//  3-sixty
//
//  Created by Mahmoud Elsayed on 11/10/2021.
//

import UIKit

class NotificationsViewCell: UITableViewCell {
    @IBOutlet weak var notificationTextLabel: UILabel!
    @IBOutlet private weak var notificationImageview: UIImageView!
    
    @IBOutlet private weak var dateLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        contentView.frame = contentView.frame.inset(by: UIEdgeInsets(top: 0, left: 20, bottom: 16, right: 20))
    }
    
    func setData(date: String,message: String){
        dateLabel.text = date
        notificationTextLabel.text = message
    }
}
