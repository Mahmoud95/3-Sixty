//
//  GoalHistoryViewController.swift
//  3-sixty
//
//  Created by Mahmoud Elsayed on 06/10/2021.
//
import UIKit
// TODO: edit get next quarter function to get prev quarters
class GoalHistoryViewController: UIViewController {

    // MARK: Outlets
    @IBOutlet weak private var goalsHistoryTableView: SelfSizedTableView!
    @IBOutlet weak private var leftArrowButton: UIButton!
    @IBOutlet weak private var rightArrowButton: UIButton!
    @IBOutlet weak private var quarterLabel: UILabel!
    
    // MARK: Variables
    private let cellID = "GoalViewCell"
    private var year = Calendar.current.component(.year, from: Date())
    private let currMonth = Calendar.current.component(.month, from: Date())
    private var quarterPointer = 0
    // this quarters array contains decremented quarters
    private var quarters:[String] = []
    
    //MARK: - Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        configTableView()
        rightArrowButton.isEnabled = false
        
        // initialize quarter list with previous quarter
        initilaizeFirstPrevQuarter()
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        // write title on top of screen
        let titleLabel = UILabel()
        titleLabel.text = "Goal History"
        navigationItem.titleView = titleLabel
        //estimate for row height of tableview
        self.goalsHistoryTableView.estimatedRowHeight = 90
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        goalsHistoryTableView.reloadData()
        
    }
    
    //MARK: - IBActions
    @IBAction func leftArrowPressed(_ sender: Any) {
        getPrevQuarterDisplayed()
    }
    
    @IBAction func rightArrowPressed(_ sender: Any) {
        getNextQuarterDisplayed()
    }
    
    // MARK: - Methods
    func configTableView(){
        let tasknib = UINib(nibName: cellID, bundle: nil)
        goalsHistoryTableView.register(tasknib, forCellReuseIdentifier: cellID)
        goalsHistoryTableView.dataSource = self
        goalsHistoryTableView.delegate = self
    }
    
    
    //helper function to get correspoding quarter to certain month
    func getQuarterNo(month:Int) -> String {
        if (1...3).contains(month) {
            return "1st Quarter (Jan - Mar)"
        }
        else if (4...6).contains(month){
            return "2nd Quarter (Apr - Jun)"
        }
        else if (7...9).contains(month){
            return "3rd Quarter (Jul - Sep)"
        }
        else{
            return "4th Quarter (Oct - Dec)"
        }
    }

    
    func getPrevQuarterDisplayed(){
        
        // enable right arrow if disabled
        rightArrowButton.isEnabled = true
        
        // if current quarter showing is the last one in array, append prev quarter and show it
        if quarterPointer == quarters.count - 1{
            // need to decrement year if we are in the first quarter of curr year, and change label to 4th quarter
            if quarters[quarterPointer][0] == "1"{
                year -= 1
                quarters.append("\(getQuarterNo(month: 10)) \(year)")
                quarterPointer += 1
                quarterLabel.text = quarters[quarterPointer]
                // reload table view with goals coressponding to that quarter
            }
            else if quarters[quarterPointer][0] == "2"{
                quarters.append("\(getQuarterNo(month: 1)) \(year)")
                quarterPointer += 1
                quarterLabel.text = quarters[quarterPointer]
                // reload table view with goals coressponding to that quarter
            }
            else if quarters[quarterPointer][0] == "3"{
                quarters.append("\(getQuarterNo(month: 4)) \(year)")
                quarterPointer += 1
                quarterLabel.text = quarters[quarterPointer]
                // reload table view with goals coressponding to that quarter
            }
            else if quarters[quarterPointer][0] == "4"{
                quarters.append("\(getQuarterNo(month: 7)) \(year)")
                quarterPointer += 1
                quarterLabel.text = quarters[quarterPointer]
                // reload table view with goals coressponding to that quarter
            }
        }
        else{
            quarterPointer += 1
            quarterLabel.text = quarters[quarterPointer]
        }
    }
    
    func getNextQuarterDisplayed(){
        if quarterPointer == 1{
            rightArrowButton.isEnabled = false
        }
        quarterPointer -= 1
        quarterLabel.text = quarters[quarterPointer]
        
    }
    
    func initilaizeFirstPrevQuarter(){
        
        let currQuarter = ("\(getQuarterNo(month: currMonth)) \(year)")
        
        if currQuarter[0] == "1"{
                year -= 1
                quarters.append("\(getQuarterNo(month: 10)) \(year)")
                quarterLabel.text = quarters[quarterPointer]
                // reload table view with goals coressponding to that quarter
            }
            else if currQuarter[0] == "2"{
                quarters.append("\(getQuarterNo(month: 1)) \(year)")
                quarterLabel.text = quarters[quarterPointer]
                // reload table view with goals coressponding to that quarter
            }
            else if currQuarter[0] == "3"{
                quarters.append("\(getQuarterNo(month: 4)) \(year)")
                quarterLabel.text = quarters[quarterPointer]
                // reload table view with goals coressponding to that quarter
            }
            else if currQuarter[0] == "4"{
                quarters.append("\(getQuarterNo(month: 7)) \(year)")
                quarterLabel.text = quarters[quarterPointer]
                // reload table view with goals coressponding to that quarter
            }
        
        else{
            quarterPointer += 1
            quarterLabel.text = quarters[quarterPointer]
        }
    }
    
}



extension GoalHistoryViewController:UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 6    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: GoalViewCell = goalsHistoryTableView.dequeueReusableCell(withIdentifier:cellID ) as! GoalViewCell
        return cell
    }
}
    extension GoalHistoryViewController: UITableViewDelegate{
        func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
            return 100.00
        }
    }
