//
//  MyGoalsViewController.swift
//  3-sixty
//
//  Created by Mahmoud Elsayed on 29/09/2021.
//

import UIKit


class MyGoalsViewController: UIViewController {

    // MARK: - Outlets
    @IBOutlet weak private var leftArrowButton: UIButton!
    @IBOutlet weak private var goalsTableView: SelfSizedTableView!
    @IBOutlet weak private var youDontHaveGoalsLabel: UILabel!
    @IBOutlet weak private var addAgoalToThisQLabel: UILabel!
    @IBOutlet weak private var noGoalsImageView: UIImageView!
    @IBOutlet weak private var quarterLabel: UILabel!
    @IBOutlet weak private var rightArrowButton: UIButton!
    @IBOutlet weak private var addGoalButton: UIButton!
    
    // MARK: - Variables
    private let goalCellId = "GoalViewCell"
    private var year = Calendar.current.component(.year, from: Date())
    private let currMonth = Calendar.current.component(.month, from: Date())
    private var quarterPointer = 0
    private var quarters: [String]? = []
    private var goals: [goal] = []
    
    struct goal: Codable{
        let id: Int
        let name: String
        let difficultyPoints: Int
        enum CodingKeys: String,CodingKey{
            case id
            case name
            case difficultyPoints = "difficulty_points"
        }
    }
    
    // MARK: Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        configTableView()
        // initialize quarter list with current quarter
        quarters?.append("\(getQuarterNo(month: currMonth)) \(year)")
        quarterLabel.text = quarters?[quarterPointer]
        if quarterPointer == 0{
            leftArrowButton.isEnabled = false
        }
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        addGoalButton.layer.cornerRadius = 25.0
        // TODO
        // get all goals (API call)
        
        // show image and 2 labels if there are no goals,else if there exist goals show tableView
        viewDecider(quarter: "")
        self.goalsTableView.estimatedRowHeight = 90
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        goalsTableView.reloadData()
    }
    
    // MARK: Methods
    static func create() -> MyGoalsViewController{
        return MyGoalsViewController(nibName: "MyGoalsViewController", bundle: nil)
    }

    // decide which view to show depending if there is goals in this quarter or not
    func viewDecider (quarter: String){
        noGoalsImageView.isHidden = true
        youDontHaveGoalsLabel.isHidden = true
        addAgoalToThisQLabel.isHidden = true
        goalsTableView.isHidden = false
    }
    
    //helper function to get correspoding quarter to certain month
    func getQuarterNo(month:Int) -> String {
        if (1...3).contains(month) {
            return "1st Quarter (Jan - Mar)"
        }
        else if (4...6).contains(month){
            return "2nd Quarter (Apr - Jun)"
        }
        else if (7...9).contains(month){
            return "3rd Quarter (Jul - Sep)"
        }
        else{
            return "4th Quarter (Oct - Dec)"
        }
    }
    
    func configTableView(){
        let goalnib = UINib(nibName: goalCellId, bundle: nil)
        goalsTableView.register(goalnib, forCellReuseIdentifier: goalCellId)
        goalsTableView.dataSource = self
        goalsTableView.delegate = self
    }
    
    func getNextQurterDisplayed(){
        // enable left arrow if disabled
        leftArrowButton.isEnabled = true
        // if current quarter showing is the last one in array, append next quarter and show it
        if quarterPointer == quarters!.count - 1{
            // need to increment year if we are in the last quarter of curr year, and change label to 1st quarter
            if quarters?[quarterPointer][0] == "4"{
                year += 1
                quarters?.append("\(getQuarterNo(month: 1)) \(year)")
                quarterPointer += 1
                quarterLabel.text = quarters?[quarterPointer]
                // reload table view with goals coressponding to that quarter
            }
            // if we are in first quarter append second quarter
            else if quarters?[quarterPointer][0] == "1"{
                quarters?.append("\(getQuarterNo(month: 4)) \(year)")
                quarterPointer += 1
                quarterLabel.text = quarters?[quarterPointer]
                // reload table view with goals coressponding to that quarter
            }
            // if we are in second quarter append third quarter
            else if quarters?[quarterPointer][0] == "2"{
                quarters?.append("\(getQuarterNo(month: 7)) \(year)")
                quarterPointer += 1
                quarterLabel.text = quarters?[quarterPointer]
                // reload table view with goals coressponding to that quarter
            }
            else if quarters?[quarterPointer][0] == "3"{
                quarters?.append("\(getQuarterNo(month: 10)) \(year)")
                quarterPointer += 1
                quarterLabel.text = quarters?[quarterPointer]
                // reload table view with goals coressponding to that quarter
            }
        }
        else{
            quarterPointer += 1
            quarterLabel.text = quarters?[quarterPointer]
        }
    }
    
    func getprevQurterDisplayed(){
        if quarterPointer == 1{
            leftArrowButton.isEnabled = false
            
        }
        quarterPointer -= 1
        quarterLabel.text = quarters?[quarterPointer]
    }
    
    // MARK: - IBActions
    @IBAction private func addGoalpressed(_ sender: Any) {
        navigationController?.pushViewController(AddGoalViewController(), animated: true)
    }
    
    @IBAction private func leftButtonPressed(_ sender: Any) {
        
        getprevQurterDisplayed()
    }
    
    @IBAction private func rightButtonPressed(_ sender: Any) {
        
        getNextQurterDisplayed()
    }
}

// MARK: Extensions for TableView
extension MyGoalsViewController: UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        //  TODO: goals number from API call if not empty
        return 6
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell :GoalViewCell = goalsTableView.dequeueReusableCell(withIdentifier: goalCellId) as! GoalViewCell
        return cell
    }
}

extension MyGoalsViewController: UITableViewDelegate{
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100.0
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        // MARK: TODO send Goalid to editGoal, get Goalselected from Goals List and send its id
        
        let vc = EditGoalViewController()
        //vc.goalID = goals[IndexPath]
        vc.goalID = 4
        navigationController?.pushViewController(vc, animated: true)
    }
}
// extension to class string to access substrings easily
extension String {
    var length: Int {
        return count
    }
    subscript (i: Int) -> String {
        return self[i ..< i + 1]
    }
    func substring(fromIndex: Int) -> String {
        return self[min(fromIndex, length) ..< length]
    }
    func substring(toIndex: Int) -> String {
        return self[0 ..< max(0, toIndex)]
    }
    subscript (r: Range<Int>) -> String {
        let range = Range(uncheckedBounds: (lower: max(0, min(length, r.lowerBound)),
                                            upper: min(length, max(0, r.upperBound))))
        let start = index(startIndex, offsetBy: range.lowerBound)
        let end = index(start, offsetBy: range.upperBound - range.lowerBound)
        return String(self[start ..< end])
    }
}
