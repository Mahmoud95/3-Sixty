//
//  GoalViewCell.swift
//  3-sixty
//
//  Created by Mahmoud Elsayed on 29/09/2021.
//

import UIKit

class GoalViewCell: UITableViewCell {
    // MARK: outlets
    @IBOutlet weak private var CategoryLabel: UILabel!
    @IBOutlet weak private var diffPointsLabel: UILabel!
    @IBOutlet weak private var taskNameLabel: UILabel!
    @IBOutlet weak private var progressImage: UIImageView!
    @IBOutlet weak private var goalNameLabel: UILabel!
    
    override func layoutSubviews() {
        super.layoutSubviews()
        contentView.frame = contentView.frame.inset(by: UIEdgeInsets(top: 10, left: 10, bottom: 0, right: 10))
    }
    
    func setData(taskName: String, category: String, diffPoints: Int, goalName:String, progImage: UIImage){
        CategoryLabel.text = category
        taskNameLabel.text = taskName
        diffPointsLabel.text = String(diffPoints)
        progressImage.image = progImage
        goalNameLabel.text = goalName
    }
}
