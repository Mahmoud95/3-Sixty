//
//  TableCellViewController.swift
//  3-sixty
//
//  Created by Ali Amer on 03/10/2021.
//

import UIKit

class TableCellViewController: UITableViewCell {
  
   
   
    @IBOutlet weak var taskLabel: UILabel!
    

    func display(task: String) {
       
        self.taskLabel.text = task
    }
    
    



}
