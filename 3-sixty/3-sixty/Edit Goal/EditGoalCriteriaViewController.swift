//
//  EditGoalCriteriaViewController.swift
//  3-sixty
//
//  Created by Ali Amer on 03/10/2021.
//

import UIKit

class EditGoalCriteriaViewController: UIViewController {
    @IBOutlet weak var communityBtn: UIButton!
    @IBOutlet weak var otherBtn: UIButton!
    @IBOutlet weak var skillsBtn: UIButton!
    @IBOutlet weak var processBtn: UIButton!
    @IBOutlet weak var saveButton: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        _ = Bundle.main.loadNibNamed("EditGoalCriteriaViewController", owner: self, options: nil)?[0] as? UIView

        let col1 = UIColor(red: 0, green: 0.5, blue: 1, alpha: 1)
        saveButton.backgroundColor = col1
        saveButton.layer.cornerRadius=20
        saveButton.setTitleColor(UIColor.white, for: .normal)
        title = "Edit Goal"
    }
    
    static func create() -> EditGoalCriteriaViewController{
        return EditGoalCriteriaViewController(nibName: "EditGoalCriteriaViewController", bundle: nil)
    }


    @IBAction func communtiyBtnAction(_ sender: UIButton) {
        if(sender.isSelected){
            sender.isSelected = false
            processBtn.isSelected = false
            skillsBtn.isSelected = false
            otherBtn.isSelected = false
        }
        else{
            sender.isSelected = true
            processBtn.isSelected = false
            skillsBtn.isSelected = false
            otherBtn.isSelected = false
            
            
        }
    }
    
    @IBAction func processBtnAction(_ sender: UIButton) {
        if(sender.isSelected){
            sender.isSelected = false
            otherBtn.isSelected = false
            skillsBtn.isSelected = false
            communityBtn.isSelected = false

            
        }
        else{
            sender.isSelected = true
            otherBtn.isSelected = false
            skillsBtn.isSelected = false
            communityBtn.isSelected = false
            
        }
    }
    
    @IBAction func skillsBtnAction(_ sender: UIButton) {
        if(sender.isSelected){
            sender.isSelected = false
            communityBtn.isSelected = false
            otherBtn.isSelected = false
            processBtn.isSelected = false
        }
        else{
            sender.isSelected = true
            communityBtn.isSelected = false
            otherBtn.isSelected = false
            processBtn.isSelected = false
        }
    }
    
    @IBAction func othersBtnAction(_ sender: UIButton) {
        if(sender.isSelected){
            sender.isSelected = false
            processBtn.isSelected = false
            communityBtn.isSelected = false
            skillsBtn.isSelected = false
        }
        else{
            sender.isSelected = true
            processBtn.isSelected = false
            communityBtn.isSelected = false
            skillsBtn.isSelected = false
        }
    }
    
    @IBAction func SaveButtonTapped(_ sender: Any) {
//        performSegue(withIdentifier: "GoToEditGoal", sender:self)
        
        _ = navigationController?.popToRootViewController(animated: true)
        
    }
    
}
