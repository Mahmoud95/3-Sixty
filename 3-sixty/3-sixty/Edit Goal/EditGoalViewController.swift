//
//  EditGoalViewController.swift
//  3-sixty
//
//  Created by Ali Amer on 30/09/2021.
//

import UIKit

class EditGoalViewController: UIViewController,UIPickerViewDelegate,UIPickerViewDataSource,UITableViewDelegate {
    var pickerIdentifier: String?
    @IBOutlet weak var TaskStatus: UITextField!
    
    @IBOutlet weak var table: UITableView!
    private let goalCellID = "TableCellViewController"
    @IBOutlet weak var Percentage: UIImageView!
    
   
    var TaskStatuspickerView = UIPickerView()
    
    
    private var TODOTasks = ["train for hackathlon"]
    
    private var InprogressTasks = ["read about hackathlon"]
    
    private var DoneTasks = ["register for the hackathlon"]
    
    
    var TaskStatusChoices = ["TODO","In Progress","Done"]
    
    
    
    static func create() -> EditGoalViewController{
        return EditGoalViewController(nibName: "EditGoalViewController", bundle: nil)
    }
    
    override func viewDidLoad() {
        _ = Bundle.main.loadNibNamed("EditGoalViewController", owner: self, options: nil)?[0] as? UIView
        super.viewDidLoad()
        addPercentage()
        table.delegate=self
        table.dataSource=self
        TaskStatuspickerView.delegate = self
        TaskStatuspickerView.dataSource = self
        TaskStatus.inputView = TaskStatuspickerView
        TaskStatus.placeholder = "Choose Task Status"
        TaskStatus.textAlignment = .center
        configTableView()
        title = "Goal"
        
        
        TaskStatusChoices = ["TODO"+"("+String(TODOTasks.count)+")" ,"In Progress"+"("+String(InprogressTasks.count)+")","Done"+"("+String(DoneTasks.count)+")"]
        
    
        
    }
    func configTableView() {
        let goalNib  = UINib(nibName: goalCellID, bundle: nil)
        table.register(goalNib, forCellReuseIdentifier: goalCellID)
        table.delegate = self
    }
    
    func addPercentage(){
               let yourImage: UIImage = UIImage(named: "78Percent")!
               Percentage.image = yourImage
           }
    
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int{
        return 1;
    }

    
    // returns the # of rows in each component..
 
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int

    {
        
        return TaskStatusChoices.count
       
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return TaskStatusChoices[row]
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        TaskStatus.text = String(TaskStatusChoices[row])
        pickerIdentifier = TaskStatusChoices[row]
        
        TaskStatus.resignFirstResponder()
    }
    
    
   
    @IBAction func addTaskPressed(_ sender: Any) {
        let taskview = AddTaskViewController.create()
        self.navigationController?.present(taskview, animated: true)
        
    }
    
   
    
    
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100.0
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("indexPath: ", indexPath)
    }

  
    @IBAction func tappedEditButton(_ sender: Any) {
        performSegue(withIdentifier: "ToEditGoalCriteria", sender:self)
        
    }
    
}

extension EditGoalViewController:UITableViewDataSource{
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: TableCellViewController =  tableView.dequeueReusableCell(withIdentifier:goalCellID) as!
            TableCellViewController
        
//        if(pickerIdentifier == "TODO"){
            
            
            
            
            let taskName = TODOTasks[indexPath.row]
            
     
            
       cell.display(task: taskName)
        tableView.reloadData()
          
          
        //}
//        else if (pickerIdentifier == "In Progress"){
//
//            let taskName = InprogressTasks[indexPath.row]
//
//            cell.display(taskLabel:taskName)
//            tableView.reloadData()
//
//        }
        
//        else if (pickerIdentifier == "Done"){
//            let taskName = DoneTasks[indexPath.row]
//
//            cell.display(taskLabel:taskName)
//            tableView.reloadData()
//
//        }
        
        
        
        
        

        


        
        return cell
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//        if(pickerIdentifier == "TODO"){
//            print("1")
//            tableView.reloadData()
//            return TODOTasks.count
//        }
//        else if (pickerIdentifier == "In Progress"){
//            print("2")
//            tableView.reloadData()
//            return InprogressTasks.count
//        }
        
//        else if (pickerIdentifier == "Done"){
//            print("3")
//            self.tableView.reloadData()
//            return DoneTasks.count
//        }
        
        
        
         //print("4")
        
           return 0
        
    }
    
    
    
}
